export default {
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/data-message/tools/schemas/1.0/sdmx-json-data-schema.json',
    id: 'IT1001',
    prepared: '2018-03-11T14:30:47',
    test: true,
    'content-languages': ['en'],
    sender: {
      id: 'ISTAT',
      name: {
        en: 'Italian Statistical Institute',
      },
    },
    receiver: {
      id: 'ESTAT',
      name: {
        en: 'Eurostat',
      },
    },
    links: [
      {
        href:
          'http://dotstat-sdg-nsiws.azurewebsites.net/rest/data/MA_545,MILLED_RICE,1.0/ASIKHM002+ASIKHM001../?dimensionAtObservation=AllDimensions&startPeriod=2013&endPeriod=2018',
        rel: 'self',
      },
    ],
  },
  data: {
    dataSets: [
      {
        action: 'Information',
        observations: {
          '0:0:0': [350.154, 0],
          '0:1:0': [389.385, 1],
          '0:2:0': [395.729, 2],
          '0:3:0': [433.638, 3],
          '1:0:0': [442.996, 0],
          '1:1:0': [426.588, 1],
          '1:2:0': [479.686, 2],
          '1:3:0': [522.296, 3],
        },
      },
    ],
    structure: {
      links: [
        {
          href: 'http://dotstat-sdg-nsiws.azurewebsites.net/rest/datastructure/MA_545/AGRI_DSD/1.0',
          urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=MA_545:AGRI_DSD(1.0)',
          rel: 'structure',
        },
        {
          href: 'http://dotstat-sdg-nsiws.azurewebsites.net/rest/dataflow/MA_545/MILLED_RICE/1.0',
          urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=MA_545:MILLED_RICE(1.0)',
          rel: 'dataflow',
        },
      ],
      name: {
        en: 'Milled rice',
      },
      description: {
        en: 'Milled rice description',
      },
      dimensions: {
        dataset: [
          {
            id: 'FREQ',
            name: {
              en: 'Frequency',
            },
            keyPosition: 2,
            role: 'FREQ',
            values: [
              {
                id: 'A',
                name: {
                  en: 'Annual',
                },
                links: [
                  {
                    href:
                      'http://dotstat-sdg-nsiws.azurewebsites.net/rest/codelist/MA_545/FREQ/1.0/A',
                    urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=MA_545:FREQ(1.0).A',
                    rel: 'self',
                  },
                ],
              },
            ],
            links: [
              {
                href: 'http://dotstat-sdg-nsiws.azurewebsites.net/rest/codelist/MA_545/FREQ/1.0',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=MA_545:FREQ(1.0)',
                rel: 'self',
              },
            ],
          },
        ],
        series: [],
        observation: [
          {
            id: 'REF_AREA',
            name: {
              en: 'Reference area',
            },
            keyPosition: 0,
            role: ['REF_AREA'],
            values: [
              {
                id: 'ASIKHM001',
                name: {
                  en: 'Banteay Meanchey',
                },
                parent: 'ASIKHM',
                order: 2,
                links: [
                  {
                    href:
                      'http://dotstat-sdg-nsiws.azurewebsites.net/rest/codelist/MA_545/CL_PROVINCES/1.0/ASIKHM001',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=MA_545:CL_PROVINCES(1.0)ASIKHM001',
                    rel: 'self',
                  },
                ],
                annotations: [0],
              },
              {
                id: 'ASIKHM002',
                name: {
                  en: 'Battambang',
                },
                parent: 'ASIKHM',
                order: 1,
                links: [
                  {
                    href:
                      'http://dotstat-sdg-nsiws.azurewebsites.net/rest/codelist/MA_545/CL_PROVINCES/1.0/ASIKHM002',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=MA_545:CL_PROVINCES(1.0).ASIKHM',
                    rel: 'self',
                  },
                ],
                annotations: [1],
              },
              {
                id: 'ASIKHM',
                name: {
                  en: 'Cambodia',
                },
                order: 0,
                links: [
                  {
                    href:
                      'http://dotstat-sdg-nsiws.azurewebsites.net/rest/codelist/MA_545/CL_PROVINCES/1.0/ASIKHM',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=MA_545:CL_PROVINCES(1.0).ASIKHM',
                    rel: 'self',
                  },
                ],
              },
            ],
            links: [
              {
                href:
                  'http://dotstat-sdg-nsiws.azurewebsites.net/rest/codelist/MA_545/CL_PROVINCES/1.0',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=MA_545:CL_PROVINCES(1.0)',
                rel: 'self',
              },
            ],
          },
          {
            id: 'TIME_PERIOD',
            name: {
              en: 'Time Period',
            },
            keyPosition: 1,
            role: ['TIME_PERIOD'],
            values: [
              {
                start: '2014-01-01',
                end: '2014-12-31',
                id: '2014',
                name: {
                  en: '2014',
                },
              },
              {
                start: '2015-01-01',
                end: '2015-12-31',
                id: '2015',
                name: {
                  en: '2015',
                },
              },
              {
                start: '2016-01-01',
                end: '2016-12-31',
                id: '2016',
                name: {
                  en: '2016',
                },
              },
              {
                start: '2017-01-01',
                end: '2017-12-31',
                id: '2017',
                name: {
                  en: '2017',
                },
              },
            ],
          },
          {
            id: 'TOTO',
            name: {
              en: 'Toto',
            },
            keyPosition: 1,
            role: ['TIME_PERIOD'],
            values: [
              {
                id: 'toto_value',
                name: {
                  en: 'toto value',
                },
              },
            ],
          },
        ],
      },
      attributes: {
        dataset: [
          {
            id: 'UNIT_MEASURE',
            name: {
              en: 'Unit of measure',
            },
            role: ['UNIT_MEASURE'],
            relationship: {
              none: {},
            },
            values: [
              {
                id: 'TONES',
                name: {
                  en: 'Tones',
                },
              },
            ],
          },
          {
            id: 'UNIT_MULT',
            name: {
              en: 'Unit multiplier',
            },
            role: ['UNIT_MULT'],
            relationship: {
              none: {},
            },
            values: [
              {
                id: '3',
                name: {
                  en: 'Thousands',
                },
              },
            ],
          },
          {
            id: 'BASE_PER',
            name: {
              en: 'Base Period',
            },
            role: ['BASE_PER'],
            relationship: {
              none: {},
            },
            values: [
              {
                id: '2010_100',
                name: {
                  en: '2010=100',
                },
              },
            ],
          },
          {
            id: 'PREF_SCALE',
            name: {
              en: 'Preferred scale',
            },
            role: ['PREF_SCALE'],
            relationship: {
              none: {},
            },
            values: [
              {
                id: '-3',
                name: {
                  en: 'Thousandth',
                },
              },
            ],
          },
          {
            id: 'DECIMALS',
            name: {
              en: 'Decimals',
            },
            role: ['DECIMALS'],
            default: '1',
            relationship: {
              none: {},
            },
            values: [
              {
                id: '1',
                name: {
                  en: 'One decimal',
                },
              },
            ],
          },
        ],
        series: [],
        observation: [
          {
            id: 'SOURCE',
            name: {
              en: 'Source',
            },
            attributeRelationship: {
              dimensions: ['TIME_PERIOD'],
            },
            values: [
              {
                name: {
                  en: 'MAFF_Agricultural Statistics_2014',
                },
              },
              {
                name: {
                  en: 'MAFF_Agricultural Statistics_2015',
                },
              },
              {
                name: {
                  en: 'MAFF_Agricultural Statistics_2016',
                },
              },
              {
                name: {
                  en: 'MAFF_Agricultural Statistics_2017',
                },
              },
            ],
          },
          {
            id: 'OBS_STATUS',
            name: {
              en: 'Observation status',
            },
            roles: ['OBS_STATUS'],
            default: 'A',
            relationship: {
              primaryMeasure: 'OBS_VALUE',
            },
            values: [
              {
                id: 'A',
                name: {
                  en: 'Normal value',
                },
              },
            ],
          },
        ],
      },
      annotations: [
        {
          title: 'Hierarchical name',
          type: 'Alternative name',
          text: {
            en: 'Banteay Meanchey',
          },
          id: 'ALT_NAME',
        },
        {
          title: 'Hierarchical name',
          type: 'Alternative name',
          text: {
            en: 'Battambang',
          },
          id: 'ALT_NAME',
        },
      ],
    },
  },
};

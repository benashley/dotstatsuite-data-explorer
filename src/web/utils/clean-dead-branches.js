import * as R from 'ramda';

const addParentWithoutData = (list, originalListIndexById) => {
  let listIndexById = R.indexBy(R.prop('id'), list);
  return R.reduce((acc, item) => {
    const parentId = R.prop('parentId', item);
    if (R.has(parentId, listIndexById)) return R.append(item)(acc);
    if (parentId) {
      const parent = R.pipe(R.prop(parentId), R.assoc('hasChild', true))(originalListIndexById);
      listIndexById = { [parent.id]: parent, ...listIndexById };
      return R.pipe(
        R.append(item),
        R.ifElse(R.always(R.has('id', parent)), R.append(parent), R.identity)
      )(acc)
    }
    return R.append(item)(acc);
  }, [], list)
};
const getNewList = (list, length, originalListIndexById, previousList = []) => {
  // stop condition;
  if (R.equals(length, R.length(previousList))) return list;
  const newList = addParentWithoutData(list, originalListIndexById);
  return getNewList(newList, R.length(newList), originalListIndexById, list);
}

export const cleanDeadBranches = (flatlist, accessor = R.prop('hasData')) => {
  const originalListIndexById = R.indexBy(R.prop('id'), flatlist);
  return R.pipe(
    R.filter(accessor),
    (list) => getNewList(list, R.length(list), originalListIndexById)
   )(flatlist)
};
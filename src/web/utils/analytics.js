import ReactGA from 'react-ga';
import * as R from 'ramda';

//----------------------------------------------------------------------------------------initialize
export const initialize = ({ token }) => {
  if (R.or(R.isEmpty(token), R.isNil(token))) return;
  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line no-console
    console.info(`google-analytics initialized with ${token}`);
  }
  ReactGA.initialize(token, { debug: true /*process.env.NODE_ENV === 'development'*/ });
};

export const joinDataflowIds = R.pipe(
  R.props(['datasourceId', 'dataflowId', 'agencyId', 'version']),
  R.join('/')
);


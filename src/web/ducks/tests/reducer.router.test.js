import bs62 from 'bs62';
import md5 from 'md5';
import reducer, {
  model,
  CHANGE_LOCATION,
  CHANGE_LOCALE,
  CHANGE_TERM,
  CHANGE_CONSTRAINTS,
  CHANGE_START,
  CHANGE_DATAFLOW,
  RESET_DATAFLOW,
  CHANGE_DATAQUERY,
  CHANGE_LAST_N_OBS,
  CHANGE_FILTER,
  CHANGE_VIEWER,
  CHANGE_FREQUENCY_PERIOD,
} from '../router';
jest.mock('../../lib/settings', () => ({ getLocaleId: v => v, theme: { visFont: '' } }));

describe('router reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      location: {},
      params: {
        constraints: {},
        start: 0,
        dataAvailability: 'on',
        hasAccessibility: false
      },
    });
  });

  it('should handle CHANGE_LOCATION', () => {
    const action = {
      type: CHANGE_LOCATION,
      payload: {
        location: {},
        params: {
          locale: 'en',
          constraints: [
            [`${bs62.encode('Topics')}`, '0|Government#GOV#'],
            [`${bs62.encode('Reference area')}`, '0|Australia#AU#'],
          ],
        },
      },
    };
    const state = model();
    const expected = {
      ...state,
      location: {},
      params: {
        locale: 'en',
        constraints: {
          [md5(`${bs62.encode('Topics')}0|Government#GOV#`)]: {
            facetId: bs62.encode('Topics'),
            constraintId: '0|Government#GOV#',
          },
          [md5(`${bs62.encode('Reference area')}0|Australia#AU#`)]: {
            facetId: bs62.encode('Reference area'),
            constraintId: '0|Australia#AU#',
          },
        },
      },
    };
    expect(reducer(state, action)).toEqual(expected);
  });

  it('should handle CHANGE_CONSTRAINTS', () => {
    const action = { type: CHANGE_CONSTRAINTS, payload: { facetId: 'toto', constraintId: 'tutu' } };
    expect(reducer({}, action)).toEqual({
      params: {
        facet: 'toto',
        constraints: { [md5(`${'toto'}${'tutu'}`)]: { facetId: 'toto', constraintId: 'tutu' } },
        start: 0,
      },
    });
  });

  it('should handle CHANGE_LOCALE', () => {
    const action = {
      type: CHANGE_LOCALE,
      payload: { localeId: 'fr' },
    };
    expect(reducer({}, action)).toEqual({ params: { locale: 'fr', start: 0 } });
  });

  it('should handle CHANGE_TERM', () => {
    const action = {
      type: CHANGE_TERM,
      payload: { term: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { term: 'toto', start: 0 } });
  });

  it('should handle CHANGE_START', () => {
    const action = {
      type: CHANGE_START,
      payload: { start: 5 },
    };
    expect(reducer({}, action)).toEqual({ params: { start: 5 } });
  });

  it('should handle CHANGE_DATAFLOW', () => {
    const action = {
      type: CHANGE_DATAFLOW,
      payload: { dataflow: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { dataflow: 'toto' } });
  });

  it('should handle RESET_DATAFLOW', () => {
    const action = {
      type: RESET_DATAFLOW,
    };
    expect(reducer({}, action)).toEqual({ params: {} });
  });

  it('should handle CHANGE_DATAQUERY', () => {
    const action = {
      type: CHANGE_DATAQUERY,
      payload: { dataquery: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { dataquery: 'toto' } });
  });

  it('should handle CHANGE_LAST_N_OBS', () => {
    const action = {
      type: CHANGE_LAST_N_OBS,
      payload: { value: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { lastNObservations: 'toto' } });
  });

  it('should handle CHANGE_FILTER', () => {
    const action = {
      type: CHANGE_FILTER,
      payload: { filterId: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { filter: 'toto' } });
  });

  it('should handle CHANGE_VIEWER', () => {
    const action = {
      type: CHANGE_VIEWER,
      payload: { viewerId: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { viewer: 'toto' } });
  });

  it('should handle CHANGE_FREQUENCY_PERIOD', () => {
    const action = {
      type: CHANGE_FREQUENCY_PERIOD,
      payload: { period: 'toto' },
    };
    expect(reducer({}, action)).toEqual({ params: { period: 'toto' } });
  });
});

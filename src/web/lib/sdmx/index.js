import * as R from 'ramda';
import { sdmxPeriod as defaultSettingsPeriod, defaultFrequency } from '../settings';
import { FILENAME_MAX_LENGTH } from '../../utils/constants';

export const getFilename = R.pipe(
  R.converge(R.append, [
    R.pipe(R.prop('dataquery'), R.ifElse(R.isNil, R.always('all'), R.identity)),
    R.pipe(R.prop('identifiers'), R.props(['agencyId', 'code', 'version'])),
  ]),
  R.join('_'),
  R.slice(0, FILENAME_MAX_LENGTH),
);

export const getDefaultRouterParams = ({ params, dataquery, frequencyArtefact }) => {
  const period = R.ifElse(
    R.anyPass([R.has('startPeriod'), R.has('endPeriod')]),
    R.converge((start, end) => [start, end], [R.prop('startPeriod'), R.prop('endPeriod')]),
    R.always(defaultSettingsPeriod),
  )(params);
  return {
    ...R.pick(['lastNObservations'], params),
    dataquery,
    period,
    frequency: R.pipe(
      R.when(R.isNil, R.always('')),
      R.split('.'),
      R.view(R.lensIndex(R.prop('index')(frequencyArtefact))),
      R.ifElse(R.either(R.isEmpty, R.isNil), R.always(defaultFrequency), R.identity),
    )(dataquery),
  };
};

export const getSelectedIdsIndexed = R.pipe(
  R.indexBy(R.prop('id')),
  R.map(
    R.pipe(
      R.propOr([], 'values'),
      R.reduce((acc, value) => {
        if (R.prop('isSelected', value)) return R.append(R.prop('id', value), acc)
        return acc;
      }, [])
    )
  ),
);

export const setSelectedDimensionsValues = (dataquery, dimensions) => {
  const splitDataquery = R.split('.', dataquery);
  return R.addIndex(R.map)((dimension, index) => {
    if (R.isEmpty(dimension)) return {};
    const partialDataquery = R.nth(index)(splitDataquery)
    if (R.isNil(partialDataquery)) return {};
    const selectedIds = R.pipe(R.split('+'), (ids) => new Set(ids))(partialDataquery);
    return R.over(
      R.lensProp('values'),
      R.map(
        R.ifElse(
          R.pipe(R.prop('id'), id => selectedIds.has(id)),
          R.assoc('isSelected', true),
          R.identity
        )
      )
    )(dimension)
  })(dimensions)
}

export const getDefaultSelectionContentConstrained = (cc) => R.mapObjIndexed((values, dimensionId) => {
  if (R.not(R.has(dimensionId, cc))) return [];
  const dimension = R.prop(dimensionId)(cc)
  return R.filter(id => dimension.has(id))(values)
})

export const getOnlyHasDataDimensions = R.map((dimension) => {
  if (R.not(R.prop('hasData')(dimension))) return {};
  return R.over(R.lensProp('values'), R.filter(R.prop('hasData')))(dimension)
})
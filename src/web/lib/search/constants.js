export default {
  FACET_LEVEL_SEPARATOR: '|',
  FACET_HIGHLIGHT_SEPARATOR: ' > ',
  FACET_VALUE_MASK: /#(.*?)#/g,
  VALUE_MASK: /#(.*?)#/,
  DATASOURCE_ID: 'datasourceId',
  DATAFLOW_ID: 'dataflowId',
};

import bs62 from 'bs62';
import * as R from 'ramda';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import { defaultFrequency } from '../settings';

const matchConstraintId = R.match(/#(.*?)#/g);
export const searchConstraintsToVisConstraints = R.pipe(
  R.values,
  R.map(
    R.evolve({
      constraintId: R.ifElse(
        R.pipe(matchConstraintId, R.isEmpty),
        R.identity,
        R.pipe(matchConstraintId, R.last, R.replace(/#/g, '')), // IE doesn't get /(?<=#)(.*?)(?=#)/g
      ),
    }),
  ),
  R.groupBy(R.prop('facetId')),
);

export const searchConstraintsToVisSelection = (dimensions, constraints) => {
  const visContraints = searchConstraintsToVisConstraints(constraints);

  return R.reduce(
    (selection, dimension) => {
      // empty dimension is possible, don't remember the usecase (cf dimitri)
      if (R.isEmpty(dimension)) return selection;

      const dimensionLabelEncoded = R.pipe(R.prop('label'), bs62.encode)(dimension);

      if (R.not(R.has(dimensionLabelEncoded, visContraints))) return selection;

      const dimensionSelection = R.pipe(
        R.prop(dimensionLabelEncoded),
        R.pluck('constraintId'),
      )(visContraints);

      return R.assoc(R.prop('id', dimension), dimensionSelection, selection);
    },
    {},
    dimensions,
  );
};

export const getDefaultSelection = (dimensions, structureSelection, constraints) => {
  const searchSelection = R.merge(structureSelection, searchConstraintsToVisSelection(dimensions, constraints));
  const freqDim = R.find(SDMXJS.isFrequency, dimensions) || [];
  
  return R.pipe(
    R.reduce((acc, dimension) => {
      const id = R.prop('id')(dimension);
      const selectedValues = R.prop(id)(searchSelection);
      if (R.or(R.isNil(id), R.isNil(selectedValues))) return acc;
      const valueIds = new Set(R.pipe(R.propOr([], 'values'), R.pluck('id'))(dimension));
      return R.assoc(id, R.filter(id => valueIds.has(id))(selectedValues))(acc);
    }, {}),
    R.reject(R.isEmpty),
    R.cond([
      [R.always(R.isEmpty(freqDim)), R.identity],
      [
        R.has(R.prop('id')(freqDim)),
        R.over(R.lensProp(R.prop('id')(freqDim)), R.pipe(R.head, R.of)),
      ],
      [
        R.T,
        R.assoc(
          R.prop('id')(freqDim),
          R.find(R.propEq('id', defaultFrequency), freqDim)
            ? [defaultFrequency]
            : [R.path(['values', 0, 'id'])(freqDim)],
        ),
      ],
    ]),
  )(dimensions);
};

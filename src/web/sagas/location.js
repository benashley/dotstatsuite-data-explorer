import { put, select, take, takeLatest } from 'redux-saga/effects';
import * as R from 'ramda';
import { CHANGE_LOCATION } from '../ducks/router';
import { requestSearch } from '../ducks/search';
import { REQUEST_DATA } from '../ducks/sdmx';
import { AUTHENTICATED } from '../ducks/user';
import { getIsAuthenticating } from '../selectors/user';

const isDev = process.env.NODE_ENV === 'development';

function* onChangeLocation(action) {
  const isAuthenticating = yield select(getIsAuthenticating);
  if (isAuthenticating) {
    yield take(AUTHENTICATED);
  }
  const request = R.prop('request', action);
  if (R.isNil(request)) {
    return;
  }
  switch (request) {
    case 'getSearch':
      yield put(requestSearch());
      break;
    case 'getData':
      yield put({ type: REQUEST_DATA });
      break;
    case 'getStructure':
      yield put({ type: REQUEST_DATA, shouldRequestStructure: true });
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`requestMiddleware: unknown request ${request}`);
      return;
  }
}

export function* locationSaga() {
  yield takeLatest(CHANGE_LOCATION, onChangeLocation);
}

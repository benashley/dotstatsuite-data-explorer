import * as R from 'ramda';
import ReactGA from 'react-ga';
import { DOWNLOAD_EXCEL_SUCCESS, SHARE_SUCCESS, DOWNLOAD_PNG_CHART_SUCCESS } from '../ducks/vis';
import { REQUEST_DATA, REQUEST_DATAFILE, HANDLE_STRUCTURE  } from '../ducks/sdmx';
import { CHANGE_LOCATION } from '../ducks/router';
import { requestSearch } from '../ducks/search';
import { getQuery } from '../utils/router';
import { getParams, getDataflow, getViewer } from '../selectors/router';
import { getDataflowName } from '../selectors/sdmx';
import { joinDataflowIds } from '../utils/analytics';

const isDev = process.env.NODE_ENV === 'development';

export const historyMiddleware = history => store => next => action => {
  if (R.not(R.has('pushHistory', action))) return next(action);

  const future = next(action);

  const search = getQuery(getParams(store.getState()));

  history.push({
    pathname: R.prop('pushHistory', action),
    search,
  });

  // eslint-disable-next-line no-console
  if (isDev) console.info(`historyMiddleware: ${action.type}`);

  return future;
};

export const requestMiddleware = store => next => action => {
  const request = R.prop('request', action);
  const future = next(action);

  if (R.isNil(request) || action.type === CHANGE_LOCATION) return future;

  switch (request) {
    case 'getSearch':
      store.dispatch(requestSearch());
      break;
    case 'getData':
      store.dispatch({ type: REQUEST_DATA });
      break;
    case 'getStructure':
      store.dispatch({ type: REQUEST_DATA, shouldRequestStructure: true });
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`requestMiddleware: unknown request ${request}`);
      return future;
  }

  // eslint-disable-next-line no-console
  if (isDev) console.info(`requestMiddleware: ${action.type} -> ${request}`);

  return future;
};

const analyticsAction = new Set([
  REQUEST_DATAFILE,
  DOWNLOAD_EXCEL_SUCCESS,
  SHARE_SUCCESS,
  HANDLE_STRUCTURE,
  DOWNLOAD_PNG_CHART_SUCCESS
]);

export const analyticsMiddleware = ({ getState }) => next => action => {
  if (analyticsAction.has(action.type)) {
    const future = next(action);
    // eslint-disable-next-line no-console
    if (isDev) console.info(`analyticsMiddleware: ${action.type}`);
    const { payload, type } = action;
    const dataflowIdentifier = R.join(' ')([getDataflowName(getState()), joinDataflowIds(getDataflow(getState()))]);

    switch (type) {
      case REQUEST_DATAFILE:
        return ReactGA.event({
          category: 'DOWNLOAD',
          action: R.prop('isDownloadAllData')(payload) ? 'CSV_FULL' : 'CSV',
          label: R.ifElse(
            R.hasPath(['dataflow', 'name']),
            R.converge(R.pipe(R.prepend, R.join(' ')), [R.path(['dataflow', 'name']), R.pipe(R.prop('dataflow'), joinDataflowIds, R.of)]),
            R.always(dataflowIdentifier)
          )(payload)
        });
      case DOWNLOAD_EXCEL_SUCCESS:
        return ReactGA.event({
          category: 'DOWNLOAD',
          action: 'EXCEL',
          label: dataflowIdentifier
        });
      case DOWNLOAD_PNG_CHART_SUCCESS:
        return ReactGA.event({
          category: 'DOWNLOAD',
          action: 'PNG',
          label: dataflowIdentifier
        });
      case SHARE_SUCCESS:
        return ReactGA.event({
          category: 'SHARE',
          action: R.toUpper(getViewer(getState())),
          label: dataflowIdentifier
        });
      case HANDLE_STRUCTURE:
        return ReactGA.event({
          category: 'DATAFLOW',
          action: 'VIEW',
          label: dataflowIdentifier,
          nonInteraction: true,
        });

    }
    return future;
  }
  return next(action);
};
import { createMuiTheme } from '@material-ui/core/styles';

export const highlight1 = '#f7a42c'; // orange
const highlight2 = '#8CC841'; // green
const highlight3 = '#fffc02'; // yellow
const primaryMain = '#0549ab' // blue
const hover = 'rgba(14, 144, 224, 0.13)'; // blue
const selected = 'rgba(14, 144, 224, 0.25)'; // blue
const textLight = '#444444';

export const theme = (rtl = 'ltr') =>
  createMuiTheme({
    direction: rtl,
    overrides: {
      MuiButton: {
        root: {
          textTransform: 'none',
        },
        text: {
          '&:hover': {
            backgroundColor: `${hover} !important`,
          },
        },
      },
      MuiMenuItem: {
        root: {
          color: primaryMain,
          '&$selected': {
            backgroundColor: hover,
            '&:hover': {
              backgroundColor: selected,
            },
          }
        }
      }
    },
    palette: {
      action: {
        active: 'rgba(14, 144, 144, 0.54)',
        selected,
        hover,
      },
      primary: {
        main: primaryMain,
        light: '#0e90e0',
        dark: primaryMain,
      },
      secondary: {
        main: '#e3e9ed',
        light: '#f5f8fa',
        dark: '#ebf1f5',
      },
      tertiary: {
        main: '#0965c1',
        light: '#e2f2fb',
        dark: '#b7def6',
      },
      highlight: {
        hl1: highlight1,
        hl2: highlight2,
        hl3: highlight3,
      },
      grey: {
        200: '#F3F7FB',
        300: '#cccccc',
        700: '#666666',
        800: textLight,
        A700: '#182026',
      },
    },
    mixins: {
      dataflow: {
        fontWeight: 400,
        fontFamily: "'Roboto Slab', serif",
      },
      scopeList: {
        fontWeight: 700,
        fontFamily: "'PT Sans Narrow', 'Helvetica Neue', Helvetica, Arial, sans-serif",
      },
      excel: {
        headerFont: '#ffffff',
        sectionFont: '#000000',
        rowFont: '#000000',
      },
      tag: {
        fontSize: 12,
        fontFamily: "'PT Sans Narrow', 'Helvetica Neue', Helvetica, Arial, sans-serif",
        color: textLight,
      },
      labelDivider: {
        fontSize: 13,
      },
      sisccButton: {
        '&:hover': {
          backgroundColor: highlight1,
        },
      },
      tableFlag: {
        fontFamily: "'PT Sans Narrow', 'Helvetica Neue', Helvetica, Arial, sans-serif",
      },
    },
    typography: {
      fontFamily: "'Helvetica Neue', Helvetica, Arial, sans-serif",
      h6: {
        fontSize: ' 1.0625rem',
      },
    },
  });

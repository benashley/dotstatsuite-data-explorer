import * as R from 'ramda';

export const getPosition = (row, column) => {
  let id = '';
  for (let a = 1, b = 26; (column -= a) >= 0; a = b, b *= 26) {
    id = String.fromCharCode(parseInt((column % b) / a) + 65) + id;
  }
  return `${id}${row}`;
};

export const formatCellValue = R.ifElse(R.has('intValue'), R.prop('intValue'), R.prop('label'));

export const formatFlags = R.pipe(R.pluck('label'), R.join('\n'));

export const formatSection = R.map(entry => ({
  label: `${R.pathOr('', ['dimension', 'label'], entry)}: ${R.pathOr(
    '',
    ['value', 'label'],
    entry,
  )}`,
  flags: R.pathOr([], ['value', 'flags'], entry),
}));

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import * as R from 'ramda';
import { getIsAuthenticating } from '../../selectors/user';
import { getIsFull } from '../../selectors';
import App from './App';

export default R.compose(
  connect(
    createStructuredSelector({
      isAuthenticating: getIsAuthenticating,
      isFull: getIsFull(),
    }),
  ),
)(App);

import * as R from 'ramda';
import html2canvas from 'html2canvas';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps } from 'recompose';
import ToolBar from './visions/DeToolBar';
import {
  changeActionId,
  changeDimensionGetter,
  downloadExcel,
  downloadPng,
  changeFullscreen,
} from '../ducks/vis';
import { changeViewer } from '../ducks/router';
import { requestDataFile } from '../ducks/sdmx';
import { getViewer, getDataflow } from '../selectors/router';
import { getIsPending } from '../selectors/app';
import { getRefAreaDimension, getExternalResources } from '../selectors/sdmx';
import {
  getIsFull,
  getVisDimensionGetter,
  getIsComputingFile,
  getIsSharing,
  getVisActionId,
  getIsOpeningFullscreen,
} from '../selectors';
import { ID_VIEWER_COMPONENT } from '../css-api';

export const DOWNLOAD = 'download';

export default compose(
  connect(
    createStructuredSelector({
      isDownloading: getIsPending('requestingDataFile'),
      isComputingExcel: getIsComputingFile('excel'),
      isComputingPng: getIsComputingFile('png'),
      dataflow: getDataflow,
      refAreaDimension: getRefAreaDimension,
      externalResources: getExternalResources,
      dimensionGetter: getVisDimensionGetter(),
      viewerId: getViewer,
      actionId: getVisActionId(),
      isSharing: getIsSharing(),
      isFull: getIsFull(),
      isOpening: getIsOpeningFullscreen,
    }),
    {
      changeViewer,
      changeActionId,
      requestDataFile,
      downloadExcel,
      downloadPng,
      changeDimensionGetter,
      changeFullscreen,
    },
  ),
  withProps(
    ({
      requestDataFile,
      downloadExcel,
      dataflow,
      viewerProps,
      theme,
      refAreaDimension,
      viewerId,
      downloadPng,
      isComputingExcel,
      isComputingPng,
      isDownloading,
    }) => {
      const defaultDownload = [
        { key: 'csv.selection', handler: () => requestDataFile({ dataflow }) },
        { key: 'csv.all', handler: () => requestDataFile({ isDownloadAllData: true, dataflow }) },
      ];

      const download = R.cond([
        [
          R.equals('table'),
          R.always(
            R.prepend(
              { key: 'excel.selection', handler: () => downloadExcel({ ...viewerProps, theme }) },
              defaultDownload,
            ),
          ),
        ],
        [
          R.T,
          R.always(
            R.prepend(
              {
                key: 'chart.selection',
                handler: () => {
                  const { width, height } = viewerProps?.chartOptions?.base;
                  let options = { scale: 2, scrollY: -window.scrollY };
                  if (!R.isNil(width)) options.width = width;
                  if (!R.isNil(height)) options.height = height;
                  downloadPng(() =>
                    html2canvas(document.getElementById(ID_VIEWER_COMPONENT), options),
                  );
                },
              },
              defaultDownload,
            ),
          ),
        ],
      ])(viewerId);

      return {
        download,
        hasRefAreaDimension: R.not(R.isNil(refAreaDimension)),
        isDownloading: R.any(R.identity)([isComputingExcel, isComputingPng, isDownloading]),
      };
    },
  ),
)(ToolBar);

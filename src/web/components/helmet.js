import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getIsRtl, getLocale } from '../selectors/router';
import { getDataflowName } from '../selectors/sdmx';
import { title } from '../lib/settings';

const View = ({ isRtl, lang, dataflowName }) => (
  <Helmet htmlAttributes={{ lang, dir: isRtl ? 'rtl' : 'ltr' }}>
    {R.isNil(dataflowName)
      ? <title>{title}</title>
      : <title>{`${dataflowName} • ${title}`}</title>
    }
  </Helmet>
);

View.propTypes = {
  isRtl: PropTypes.bool,
  lang: PropTypes.string,
  dataflowName: PropTypes.string,
};

export default connect(
  createStructuredSelector({
    isRtl: getIsRtl,
    lang: getLocale,
    dataflowName: getDataflowName
  }),
)(View);

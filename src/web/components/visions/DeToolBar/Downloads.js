import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import MuiButton from '@material-ui/core/Button';
import { FormattedMessage } from 'react-intl';
import { Button, Menu } from './helpers';

const useStyles = makeStyles(theme => ({
  link: {
    margin: theme.spacing(0.5, 0, 0.5, 0),
    padding: 0,
  },
  linkIcon: {
    height: 20,
  },
}));

const Component = ({ download, loading, externalResources }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<GetAppIcon />}
        selected={Boolean(anchorEl)}
        loading={loading}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.action.download" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(
          ({ key, handler }) => (
            <MenuItem key={key} onClick={itemClick(handler)} dense>
              <ListItemText primaryTypographyProps={{ color: 'primary' }}>
                <FormattedMessage id={`de.visualisation.toolbar.action.download.${key}`} />
              </ListItemText>
            </MenuItem>
          ),
          download,
        )}
        {R.not(R.isEmpty(externalResources)) && <Divider />}
        {R.not(R.isEmpty(externalResources)) &&
          R.map(
            ({ id, label, link, img }) => (
              <MenuItem key={id} dense aria-labelledby={id}>
                <MuiButton
                  component="a"
                  color="primary"
                  target="_blank"
                  href={link}
                  className={classes.link}
                  download
                  id={id}
                >
                  <img src={img} className={classes.linkIcon} alt={`${id} icon`} />
                  {label}
                </MuiButton>
              </MenuItem>
            ),
            externalResources,
          )}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  download: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  externalResources: PropTypes.array,
};

export default Component;

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'recompose';
import { withKeycloak } from 'react-keycloak';
import * as R from 'ramda';
import { getMe } from '../selectors/user';
import { FormattedMessage } from 'react-intl';
import Button from '@material-ui/core/Button';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PersonIcon from '@material-ui/icons/Person';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#F0F0F0',
  },
  tooltip: {
    margin: 0,
    padding: 0,
  },
  label: {
    fontFamily: 'Arial',
    textTransform: 'none',
    fontSize: 14,
  },
};

const User = ({ classes, keycloak, me }) => {
  if (!me) {
    return (
      <Button classes={R.pick(['label'], classes)} onClick={() => keycloak.login()}>
        <PersonIcon />
        <FormattedMessage id="user.login" />
      </Button>
    );
  }
  return (
    <Tooltip
      classes={R.pick(['tooltip'], classes)}
      interactive={true}
      PopperProps={{ placement: 'bottom' }}
      title={
        <div className={classes.content}>
          <Typography classes={R.pick(['label'], classes)}>{me.email}</Typography>
          <Button classes={R.pick(['label'], classes)} onClick={() => keycloak.logout()}>
            <ExitToAppIcon />
            <FormattedMessage id="user.logout" />
          </Button>
        </div>
      }
    >
      <Button classes={R.pick(['label'], classes)}>
        <PersonIcon />
        {me.name}
      </Button>
    </Tooltip>
  );
};

User.propTypes = {
  classes: PropTypes.object,
  keycloak: PropTypes.shape({
    logout: PropTypes.func.isRequired,
  }),
  me: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
  }),
};

export default compose(
  connect(createStructuredSelector({ me: getMe })),
  withKeycloak,
  withStyles(styles),
)(User);

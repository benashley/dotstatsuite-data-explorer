import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderNothing, withProps } from 'recompose';
import * as R from 'ramda';
import { getCurrentRows, getPage, getPages } from '../../selectors/search';
import { changeStart } from '../../ducks/router';
import { injectIntl } from 'react-intl';
import { Pagination } from '@sis-cc/dotstatsuite-visions';

const mapStateToProps = createStructuredSelector({
  page: getPage,
  pages: getPages,
  rows: getCurrentRows,
});

const mapDispatchToProps = {
  changeStart,
};

// labels need to be string to support wcag
export default compose(
  injectIntl,
  connect(mapStateToProps, mapDispatchToProps),
  branch(({ pages, rows }) => R.isNil(rows) || R.isNil(pages) || pages === 1, renderNothing),
  withProps(({ changeStart, rows, intl }) => ({
    onChange: page => changeStart(rows * (page - 1)),
    onSubmit: page => changeStart(rows * (page - 1)),
    labels: {
      page: intl.formatMessage({ id: 'de.search.page' }),
      of: intl.formatMessage({ id: 'de.search.page.of' }),
      startPage: intl.formatMessage({ id: 'wcag.search.page.start' }),
      previousPage: intl.formatMessage({ id: 'wcag.search.page.previous' }),
      nextPage: intl.formatMessage({ id: 'wcag.search.page.next' }),
      endPage: intl.formatMessage({ id: 'wcag.search.page.end' }),
    },
  })),
)(Pagination);

import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { injectIntl } from 'react-intl';
import { Dataflow } from '@sis-cc/dotstatsuite-visions';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    borderTop: `2px solid ${theme.palette.grey[500]}`,
  },
}));

const Dataflows = ({ dataflows, pending, changeDataflow, requestDataFile, intl }) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {R.map(
        dataflow => (
          <Dataflow
            key={R.prop('id', dataflow)}
            id={R.prop('id', dataflow)}
            title={R.prop('name', dataflow)}
            body={{
              description: R.prop('description', dataflow),
            }}
            url={R.prop('url', dataflow)}
            handleUrl={event => {
              event.stopPropagation();
              if (event.ctrlKey) return;
              event.preventDefault();
              return changeDataflow(dataflow);
            }}
            highlights={R.prop('highlights', dataflow)}
            label={R.prop('datasourceId', dataflow)}
            labels={{
              source: intl.formatMessage({ id: 'de.search.dataflow.source' }),
              lastUpdated: intl.formatMessage({ id: 'de.search.dataflow.last.updated' }),
              download: intl.formatMessage({ id: 'de.visualisation.toolbar.action.download' }),
              readMore: 'Read more',
              note: `${R.prop('agencyId', dataflow)}:${R.prop('dataflowId', dataflow)}(${R.prop(
                'version',
                dataflow,
              )})`,
              date: intl.formatDate(R.prop('indexationDate', dataflow), {
                year: 'numeric',
                month: 'long',
                day: '2-digit',
              }),
              downloadTooltip: intl.formatMessage({
                id: 'de.visualisation.toolbar.action.download.csv.all',
              }),
            }}
            onDownload={() => requestDataFile({ isDownloadAllData: true, dataflow })}
            isDownloading={R.pipe(
              R.prop(`getDataFile/${R.prop('id', dataflow)}`),
              R.equals(true),
            )(pending)}
            {...dataflow}
          />
        ),
        dataflows,
      )}
    </div>
  );
};

Dataflows.propTypes = {
  pending: PropTypes.object,
  dataflows: PropTypes.array,
  requestDataFile: PropTypes.func.isRequired,
  changeDataflow: PropTypes.func.isRequired,
  intl: PropTypes.object,
};

export default injectIntl(Dataflows);

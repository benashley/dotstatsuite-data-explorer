import { prop } from 'ramda';
import { createSelector } from 'reselect';

const getUser = prop('user');

export const getMe = createSelector(getUser, prop('me'));

export const getTokenId = createSelector(getUser, prop('tokenId'));

export const getIsAuthenticating = createSelector(getUser, prop('authenticating'));
